import os
import tkinter
from tkinter import *
from tkinter import messagebox, filedialog
from tkinter.ttk import Notebook

from CreateBasicReportComponents import *
from CreateSnippetApp import *
from CreateYamlApp import *


class MainHandler:
    def __init__(self):
        self.outputPath = ""
        self.applicationPath = self.getApplicationPath()
        self.snippetList = []
        self.rows = []
        self.window = tkinter.Tk()
        self.window.title("Freemarker-Report Generator")
        self.window.geometry('1100x800')
        self.tabControl = Notebook(self.window)
        self.reportSpecsTab = Frame(self.tabControl)
        self.snippetCreateTab = Frame(self.tabControl)
        self.snippetDataTable = Frame(self.snippetCreateTab)
        self.snippetDataTable.grid(row=3, column=0)
        self.yamlTxtCreateTab = Frame(self.tabControl)
        self.userPreferencesTab = Frame(self.tabControl)
        self.tabControl.add(self.reportSpecsTab, text='Report specifications')
        self.tabControl.add(self.snippetCreateTab, text='Snippet specifications')
        self.tabControl.add(self.yamlTxtCreateTab, text='Create YAML-Resources')
        self.tabControl.add(self.userPreferencesTab, text='Set user-preferences')
        self.createReportSpecificationTab()
        self.createSnippetTab()
        self.createYamlTab()
        self.createUserPreferencesTab()
        self.window.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.window.mainloop()

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Do you want to quit the report-generator?", parent=self.window):
            self.window.destroy()

    def getApplicationPath(self):
        if getattr(sys, 'frozen', False): # is app
            applicationPath = os.path.dirname(sys.executable)
        else: # is in IDE
            applicationPath = os.path.dirname(__file__)
        return applicationPath

    def createReportWithSnippets(self):
        reportId = self.uniqueIdInput.get()
        reportName = self.reportNameInput.get()
        aclRoles = self.aclRolesInput.get()
        onEntity = self.entityInput.get()
        listView = self.isListChecked.get()
        detailView = self.isDetailChecked.get()
        reportLocation = []
        if self.outputPath == "":
            self.tabControl.select(self.userPreferencesTab)
            return
        if reportId != '' and reportName != '' and aclRoles != '' and onEntity != '':
            if listView:
                reportLocation.append("list")
            if detailView and listView:
                reportLocation.append(", ")
            if detailView:
                reportLocation.append("detail")
            reportGenerator = CreateBasicReportComponents(self.applicationPath,
                                                         reportId,
                                                         reportName,
                                                         "".join(reportLocation),
                                                         onEntity,
                                                         aclRoles,
                                                         self.snippetList)
            reportGenerator.execReportGenerator(self.outputPath)
        else:
            messagebox.showinfo('Please fill out all required fields', 'The application can not start, without the required fields.')


    def createSnippet(self):
        CreateSnippetApp(self, None)

    def addSnippet(self, snippet):
        self.snippetList.append(snippet)

    def editSnippet(self, currentRowIndex):
        currentSnippet = self.snippetList[currentRowIndex]
        CreateSnippetApp(self, currentSnippet)


    def deleteSnippet(self, currentRowIndex):
        askyesno = messagebox.askyesno("Delete snippet confirmation", "Do you really want to delete this snippet?")
        if askyesno:
            self.snippetList.pop(currentRowIndex)
            self.reloadSnippetOverviewTable()

    def reloadSnippetOverviewTable(self):
        self.snippetDataTable.destroy()
        self.snippetDataTable = Frame(self.snippetCreateTab)
        self.snippetDataTable.grid(row=3, column=0)
        self.createOverviewTableHeader()
        self.getAllPreparedSnippets()

    def getAllPreparedSnippets(self):
        currentRowIndex = 3
        currentListIndex = 0
        rowItems = []
        self.rows = []
        for snippet in self.snippetList:
            fieldNames = []
            if snippet.isNoSnippetInput:
                loopVar = snippet.snippetData
                snippetData = ""
            else:
                loopVar = ""
                snippetData = snippet.snippetData
            for field in snippet.snippetFields:
                fieldNames.append(field.technicalFieldName + "\n(" + field.labelDe + ")\n")
            snippetListOutput = [snippet.snippetName, snippet.snippetType, snippetData, loopVar, "".join(fieldNames)]
            for columnIndex in range(0, 5):  # Columns
                newTableRow = Label(self.snippetDataTable, text=snippetListOutput[columnIndex], width=20)
                newTableRow.grid(row=currentRowIndex, column=columnIndex)
                rowItems.append(newTableRow)
            editButton = Button(self.snippetDataTable,
                                text="Edit",
                                fg="white",
                                bg="blue",
                                font="bold",
                                command=lambda rowIndex=currentListIndex:  self.editSnippet(rowIndex))
            editButton.grid(row=currentRowIndex, column=7)
            rowItems.append(editButton)
            deleteButton = Button(self.snippetDataTable,
                                  text="Delete",
                                  fg="white",
                                  bg="red",
                                  font="bold",
                                  command=lambda rowIndex=currentListIndex:  self.deleteSnippet(rowIndex))
            deleteButton.grid(row=currentRowIndex, column=8)
            rowItems.append(deleteButton)
            self.rows.append(rowItems)
            rowItems = []
            currentRowIndex += 1
            currentListIndex += 1

    def createOverviewTableHeader(self):
        snippetNameLabel = Label(self.snippetDataTable, text="Snippet name", font='bold', width=15)
        snippetNameLabel.grid(row=1, column=0)

        snippetTypeLabel = Label(self.snippetDataTable, text="Snippet type", font='bold', width=15)
        snippetTypeLabel.grid(row=1, column=1)

        snippetDataLabel = Label(self.snippetDataTable, text="Snippet data", font='bold', width=15)
        snippetDataLabel.grid(row=1, column=2)

        snippetVarLabel = Label(self.snippetDataTable, text="Loop var", font='bold', width=15)
        snippetVarLabel.grid(row=1, column=3)

        fieldNameLabel = Label(self.snippetDataTable, text="Fieldname", font='bold', width=15)
        fieldNameLabel.grid(row=1, column=4)


    def createReportSpecificationTab(self):
        self.uniqueIdLabel = Label(self.reportSpecsTab, text="Report unique-id", font='bold')
        self.reportNameLabel = Label(self.reportSpecsTab, text="Report name", font='bold')
        self.aclRolesLabel = Label(self.reportSpecsTab, text="ACL roles", font='bold')
        self.entityLabel = Label(self.reportSpecsTab, text="Entity of report", font='bold')
        self.placedLabel = Label(self.reportSpecsTab, text="Placement of report", font='bold')

        self.uniqueIdInput = Entry(self.reportSpecsTab, width=55)
        self.uniqueIdInput.focus()
        self.reportNameInput = Entry(self.reportSpecsTab, width=55)
        self.aclRolesInput = Entry(self.reportSpecsTab, width=55)
        self.entityInput = Entry(self.reportSpecsTab, width=55)
        self.isDetailChecked = BooleanVar()
        self.isListChecked = BooleanVar()
        self.isDetailChecked.set(1)
        self.isListChecked.set(0)
        self.listEntityOption = Checkbutton(self.reportSpecsTab, text='List', var=self.isListChecked)
        self.detailEntityOption = Checkbutton(self.reportSpecsTab, text='Detail', var=self.isDetailChecked)
        self.uniqueIdLabel.grid(column=1, row=1, pady=(5, 5))
        self.reportNameLabel.grid(column=1, row=2, pady=(0, 5))
        self.aclRolesLabel.grid(column=1, row=3, pady=(0, 5))
        self.entityLabel.grid(column=1, row=4, pady=(0, 5))
        self.placedLabel.grid(column=1, row=5, pady=(0, 5))
        self.uniqueIdInput.grid(column=2, row=1)
        self.window.update()
        self.reportNameInput.grid(column=2, row=2)
        self.aclRolesInput.grid(column=2, row=3)
        self.entityInput.grid(column=2, row=4)
        self.listEntityOption.grid(column=2, row=5)
        self.detailEntityOption.grid(column=2, row=6)
        createReportButton = Button(self.reportSpecsTab,
                                    text="Create report-pattern",
                                    fg="white",
                                    bg="green",
                                    font="bold",
                                    command=self.createReportWithSnippets)
        createReportButton.grid(column=1, row=5)
        createReportButton.grid(column=1, row=0)

    def createSnippetTab(self):
        createSnippetButton = Button(self.snippetCreateTab,
                                     text="Create snippet",
                                     fg="white",
                                     bg="green",
                                     font="bold",
                                     command=self.createSnippet)
        createSnippetButton.grid(column=1, row=5)
        createSnippetButton.grid(column=1, row=0)
        self.tabControl.pack(expand=1, fill='both')
        self.createOverviewTableHeader()

    def createYamlTab(self):
        CreateYamlApp(self, self.yamlTxtCreateTab)

    def askDirectory(self, var):
        dirPath = filedialog.askdirectory()
        var.set(dirPath)
        self.outputPath = dirPath

    def createUserPreferencesTab(self):
        outputPathLabel = Label(self.userPreferencesTab, text="Output path:", font='bold')
        var = StringVar()
        outputPathInputLabel = Label(self.userPreferencesTab, width=80, textvariable=var)
        askDirButton = Button(self.userPreferencesTab,
                              text="Choose Output-Path",
                              fg="white",
                              bg="blue",
                              font="bold",
                              command=lambda: self.askDirectory(var))
        
        outputPathLabel.grid(column=0, row=1)
        outputPathInputLabel.grid(column=1, row=1)
        askDirButton.grid(column=2, row=1)
        self.tabControl.pack(expand=1, fill='both')


mainHandler = MainHandler()
