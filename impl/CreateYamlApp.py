import os
import platform
import tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.ttk import Combobox


class YamlTextResource:
    def __init__(self, uniqueId, reportResourceId, label, textValue):
        self.uniqueId = uniqueId
        self.reportResourceId = reportResourceId
        self.label = label
        self.textValue = textValue

class YamlCodeCreator:
    def __init__(self, yamlResourceList):
        self.yamlResourceList = yamlResourceList

    def createYamlResource(self, applicationPath):
        yamlEntry = self.readFile(applicationPath + "/templates/yamltemplate.yaml")
        yamlResources = []
        for yaml in self.yamlResourceList:
            yamlResources.append(yamlEntry.format(uniqueId=yaml.uniqueId,
                                              label=yaml.label,
                                              reportResourceId=yaml.reportResourceId) + "\n")
        return "".join(yamlResources)

    def createYamlPropertyResource(self, applicationPath):
        yamlPropertyEntry = self.readFile(applicationPath + "/templates/yamltextkeytemplate.properties")
        yamlTextResources = []
        yamlTextResources.append("\n")
        for yaml in self.yamlResourceList:
            yamlTextResources.append(yamlPropertyEntry.format(uniqueId=yaml.uniqueId,
                                                              labelDe=yaml.label))
            yamlTextResources.append("\n")
        return "".join(yamlTextResources)

    def readFile(self, filePath):
        file = open(filePath, "r")
        fileContent = file.read()
        file.close()
        return fileContent


class CreateYamlApp:
    def __init__(self, mainHandler, createYamlTab):
        self.mainHandler = mainHandler
        self.createYamlTab = createYamlTab
        self.rows = []
        self.yamlResourceList = []
        self.createYamlButton = Button(self.createYamlTab,
                                       text="Create yaml-text resources",
                                       fg="white",
                                       bg="green",
                                       font="bold",
                                       command=self.execYamlGenerator)
        self.createYamlButton.grid(column=0, row=0)
        self.yamlResourceTable = Frame(self.createYamlTab)
        self.yamlResourceTable.grid(row=2, column=0, pady=(10, 10))
        self.createTableHeader()
        self.addFirstYamlResource()

    def execYamlGenerator(self):
        if self.mainHandler.outputPath == "":
            self.mainHandler.tabControl.select(self.mainHandler.userPreferencesTab)
            return
        targetPath = self.mainHandler.outputPath + "/output"
        areYamlSaved = self.saveAllYamlResources()
        if areYamlSaved:
            if not os.path.exists(targetPath + "/yaml"):
                os.makedirs(targetPath + "/yaml")
            yamlGenerator = YamlCodeCreator(self.yamlResourceList)
            if os.path.exists(targetPath + "/yaml/reporttextressources.yaml"):
                file = open(targetPath + "/yaml/reporttextressources.yaml", "a")
            else:
                file = open(targetPath + "/yaml/reporttextressources.yaml", "w")
            file.write(yamlGenerator.createYamlResource(self.mainHandler.applicationPath))
            file.close()
            if os.path.exists(targetPath + "/languages.properties"):
                file = open(targetPath + "/languages.properties", "a")
            else:
                file = open(targetPath + "/languages.properties", "w")
            file.write(yamlGenerator.createYamlPropertyResource(self.mainHandler.applicationPath))
            file.close()
            if "Linux" in platform.system():
                os.system("nautilus " + targetPath + " &") # & is needed to don't freeze the app, while nautilus is open
            else:
                os.system("open " + targetPath)

    def addFirstYamlResource(self):
        firstGridRow = 1
        rowItems = []
        newSelectionRow = tkinter.ttk.Checkbutton(self.yamlResourceTable)
        newSelectionRow.state(['!alternate'])
        rowItems.append(newSelectionRow)
        newSelectionRow.grid(row=firstGridRow, column=0)
        for columnIndex in range(1, 5):  # Columns
            newTableRow = Entry(self.yamlResourceTable)
            newTableRow.bind("<KeyRelease>", lambda event=self: self.refreshVisualEntryStatus(event))
            rowItems.append(newTableRow)
            newTableRow.grid(row=firstGridRow, column=columnIndex)
        rowItems[1].focus()
        self.rows.append(rowItems)

    def addYamlResource(self):
        nextGridRow = len(self.rows) + 1
        rowItems = []
        newSelectionRow = tkinter.ttk.Checkbutton(self.yamlResourceTable)
        newSelectionRow.state(['!alternate'])
        rowItems.append(newSelectionRow)
        newSelectionRow.grid(row=nextGridRow, column=0)
        for columnIndex in range(1, 5): #Columns
            newTableRow = Entry(self.yamlResourceTable)
            newTableRow.bind("<KeyRelease>", lambda event=self: self.refreshVisualEntryStatus(event))
            rowItems.append(newTableRow)
            newTableRow.grid(row=nextGridRow, column=columnIndex)
        rowItems[1].focus()
        self.rows.append(rowItems)

    def refreshVisualEntryStatus(self, entry):
        if entry.widget.get() != "":
            entry.widget.config(bg="white")
        else:
            entry.widget.config(bg="red")

    def saveAllYamlResources(self):
        amountTxtYaml = len(self.rows)
        self.yamlResourceList = []
        hasEmptyFields = False
        hasDuplicateUniqueId = False
        yamlUniqueIdList = []
        if amountTxtYaml > 0:
            for rowIndex in range(amountTxtYaml):
                uniqueId = self.rows[rowIndex][1].get()
                label = self.rows[rowIndex][2].get()
                reportResource = self.rows[rowIndex][3].get()
                textValue = self.rows[rowIndex][4].get()
                if uniqueId == "":
                    self.rows[rowIndex][1].config(bg="red")
                    hasEmptyFields = True
                if label == "":
                    self.rows[rowIndex][2].config(bg="red")
                    hasEmptyFields = True
                if reportResource == "":
                    self.rows[rowIndex][3].config(bg="red")
                    hasEmptyFields = True
                if textValue == "":
                    self.rows[rowIndex][4].config(bg="red")
                    hasEmptyFields = True
                if self.rows[rowIndex][1].cget("state") != "readonly":
                    yamlResource = YamlTextResource(uniqueId, reportResource, label, textValue)
                    if uniqueId in yamlUniqueIdList:
                        self.rows[rowIndex][1].config(bg="orange")
                        hasDuplicateUniqueId = True
                    yamlUniqueIdList.append(uniqueId)
                    self.yamlResourceList.append(yamlResource)
            if hasEmptyFields:
                messagebox.showinfo("YAML resources are incomplete",
                                    """Please fill out every field in the YAML table""")
                self.yamlResourceList = []
                return False
            if hasDuplicateUniqueId:
                messagebox.showinfo("Duplicate unique-id of yaml-resource",
                                    """A yaml resource must be unique, to be identified by report-directives.""")
                self.yamlResourceList = []
                return False
            for rowIndex in range(amountTxtYaml):
                self.rows[rowIndex][1].config(state="readonly")
                self.rows[rowIndex][2].config(state="readonly")
                self.rows[rowIndex][3].config(state="readonly")
                self.rows[rowIndex][4].config(state="readonly")
            return True
        else:
            messagebox.showinfo('No YAML-resources available',
                                """To create some YAML-resources you need to define some of them""")
            return False

    def deleteYamlResources(self):
        askyesno = messagebox.askyesno("Delete yaml-resource confirmation", "Do you really want to delete the selected yaml-resources?")
        if askyesno:
            for rowIndex, row in reversed(list(enumerate(self.rows))):
                deleteCheckBoxState = row[0]
                if deleteCheckBoxState.instate(['selected']):
                    for cell in row:
                        cell.destroy()
                    self.rows.pop(rowIndex)

    def createTableHeader(self):
        createButton = Button(self.createYamlTab,
                              text='Add Yaml',
                              fg="white",
                              bg="green",
                              font="bold",
                              command=self.addYamlResource)
        createButton.grid(row=0, column=1)

        deleteButton = Button(self.createYamlTab,
                              text='Delete Yaml',
                              fg="white",
                              bg="red",
                              font="bold",
                              command=self.deleteYamlResources)
        deleteButton.grid(row=0, column=2)

        deleteColumnLabel = Label(self.yamlResourceTable, text="Delete selection", font='bold', width=15)
        deleteColumnLabel.grid(row=0, column=0)

        uniqueIdLabel = Label(self.yamlResourceTable, text="Unique id", font='bold', width=15)
        uniqueIdLabel.grid(row=0, column=1)

        yamlLabelLabel = Label(self.yamlResourceTable, text="Label", font='bold', width=15)
        yamlLabelLabel.grid(row=0, column=2)

        reportResourceLabel = Label(self.yamlResourceTable, text="Report resource", font='bold', width=15)
        reportResourceLabel.grid(row=0, column=3)

        yamlTextLabel = Label(self.yamlResourceTable, text="Text", font='bold', width=15)
        yamlTextLabel.grid(row=0, column=4)
