import tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.ttk import Combobox, Notebook

from impl.CreateBasicReportComponents import GenericSnippet

class Field:
    def __init__(self, technicalFieldName, labelDe):
        self.technicalFieldName = technicalFieldName
        self.labelDe = labelDe

class CreateSnippetApp:
    def __init__(self, mainHandler, currentSnippet):
        self.mainHandler = mainHandler
        self.rows = []
        self.tempFieldList = []
        self.editableSnippet = currentSnippet
        self.snippetNameOfEditableBefore = ""
        self.createSnippetWithFieldsApp = Toplevel(self.mainHandler.window)
        self.createSnippetWithFieldsApp.geometry('1000x700')
        self.tabControl = Notebook(self.createSnippetWithFieldsApp)
        self.snippetDataTab = Frame(self.tabControl)
        self.snippetFieldTab = Frame(self.tabControl)
        self.createFieldsTab()
        self.fieldTable = Frame(self.snippetFieldTab)
        self.fieldTable.grid(row=1, column=0, pady=(10, 10))
        self.tabControl.add(self.snippetDataTab, text='Snippet specifications')
        self.tabControl.add(self.snippetFieldTab, text='Snippet fields')
        self.tabControl.pack(expand=1, fill='both')
        self.snippetTypeLabel = None
        self.snippetNameLabel = None
        self.snippetDataLabel = None
        self.snippetLoopVarLabel = None
        self.snippetTypeInput = None
        self.createSnippetSettings()
        self.createTableHeader()
        if self.editableSnippet is not None:
            winTitle = "Edit snippet"
            self.addEditableFields()
        else:
            winTitle = "Create new Snippet"
            self.addFirstField()
        self.createSnippetWithFieldsApp.title(winTitle)
        self.createSnippetWithFieldsApp.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.checkSnippetInputFields()
        self.createSnippetWithFieldsApp.mainloop()

    def on_closing(self):
        if self.editableSnippet is not None:
            if messagebox.askokcancel("Discard changes?", "Do you want to skip the current edit-changes of the snippet?", parent=self.createSnippetWithFieldsApp):
                self.createSnippetWithFieldsApp.destroy()
        else:
            if messagebox.askokcancel("Don't create snippet?", "Do you want to cancel the create-snippet progress and discard this snippet?", parent=self.createSnippetWithFieldsApp):
                self.createSnippetWithFieldsApp.destroy()

    def addTempEditableFields(self):
        nextGridRow = 1
        for field in self.tempFieldList:
            self.addEditableField(field, nextGridRow)
            nextGridRow += 1

    def addEditableFields(self):
        nextGridRow = 1
        for field in self.editableSnippet.snippetFields:
            self.addEditableField(field, nextGridRow)
            nextGridRow += 1

    def addEditableField(self, field, nextGridRow):
        rowItems = []
        newSelectionRow = tkinter.ttk.Checkbutton(self.fieldTable)
        newSelectionRow.state(['!alternate'])
        newSelectionRow.grid(row=nextGridRow, column=0)
        rowItems.append(newSelectionRow)
        technicalNameEntry = Entry(self.fieldTable)
        technicalNameEntry.bind("<KeyRelease>", lambda event=self: self.refreshVisualEntryStatus(event))
        technicalNameEntry.insert(0, field.technicalFieldName)
        technicalNameEntry.grid(row=nextGridRow, column=1)
        labelDeEntry = Entry(self.fieldTable)
        labelDeEntry.bind("<KeyRelease>", lambda event=self: self.refreshVisualEntryStatus(event))
        labelDeEntry.insert(0, field.labelDe)
        labelDeEntry.grid(row=nextGridRow, column=2)
        rowItems.append(technicalNameEntry)
        rowItems.append(labelDeEntry)
        self.rows.append(rowItems)


    def addField(self):
        nextGridRow = len(self.rows) + 2
        rowItems = []
        newSelectionRow = tkinter.ttk.Checkbutton(self.fieldTable)
        newSelectionRow.state(['!alternate'])
        rowItems.append(newSelectionRow)
        newSelectionRow.grid(row=nextGridRow, column=0)
        for columnIndex in range(1, 3): #Columns
            newTableRow = Entry(self.fieldTable)
            newTableRow.bind("<KeyRelease>", lambda event=self: self.refreshVisualEntryStatus(event))
            rowItems.append(newTableRow)
            newTableRow.grid(row=nextGridRow, column=columnIndex)
        self.rows.append(rowItems)

    def saveSnippet(self):
        fieldList = []
        hasEmptyFields = False
        hasDuplicateFields = False
        self.tempFieldList = []
        snippetType = self.snippetTypeInput.get()
        snippetName = self.snippetNameInput.get()
        snippetData, isNoSnippetInputOfSnippet = self.useCorrectFieldBaseData(self.snippetDataInput.get(),
                                                                              self.snippetLoopVarInput.get())
        amountFields = len(self.rows)
        if (snippetName == '' or snippetType == '' or (snippetData == 'baseData' and snippetType != "generic_table")):
            messagebox.showinfo('Please define the snippet',
                                """To create outputtemplate-fields, the snippet-type, snippet-data and the snippet-name should be set.""",
                                parent=self.createSnippetWithFieldsApp)
            return

        for snippet in self.mainHandler.snippetList:
            if snippet.snippetName == snippetName:
                if self.editableSnippet is not None:
                    if self.editableSnippet.snippetName != snippetName:
                        messagebox.showinfo('Snippetname should be unique',
                                            """The names of snippets must be unique for the outputtemplate-fields""",
                                            parent=self.createSnippetWithFieldsApp)
                        return
                else:
                    messagebox.showinfo('Snippetname should be unique',
                                        """The names of snippets must be unique for the outputtemplate-fields""",
                                        parent=self.createSnippetWithFieldsApp)
                    return
        else:
            fieldNameList = []
            for rowIndex in range(amountFields):
                technicalFieldName = self.rows[rowIndex][1].get()
                labelDe = self.rows[rowIndex][2].get()
                if technicalFieldName == "":
                    self.rows[rowIndex][1].config(bg="red")
                    hasEmptyFields = True
                if labelDe == "":
                    self.rows[rowIndex][2].config(bg="red")
                    hasEmptyFields = True
                field = Field(technicalFieldName, labelDe)
                if technicalFieldName in fieldNameList:
                    self.rows[rowIndex][1].config(bg="orange")
                    hasDuplicateFields = True
                else:
                    fieldList.append(field)
                fieldNameList.append(technicalFieldName)
            if hasEmptyFields:
                messagebox.showinfo('Fields are (partially) empty!',
                                    """The fields which are defined should have a label and a technical name.""",
                                    parent=self.createSnippetWithFieldsApp)
                self.tabControl.select(self.snippetFieldTab)
                return
            if hasDuplicateFields:
                messagebox.showinfo('Fieldnames should be unique',
                                    """Each outputtemplate-field should have an unique technical name""",
                                    parent=self.createSnippetWithFieldsApp)
                self.tabControl.select(self.snippetFieldTab)
                return
            snippet = GenericSnippet(snippetType, snippetName, snippetData, isNoSnippetInputOfSnippet, fieldList)
            for savedSnippet in self.mainHandler.snippetList:
                if savedSnippet.snippetName == self.snippetNameOfEditableBefore:
                    self.mainHandler.snippetList.remove(savedSnippet)
            self.mainHandler.addSnippet(snippet)
            self.mainHandler.reloadSnippetOverviewTable()
            self.createSnippetWithFieldsApp.destroy()

    def refreshVisualEntryStatus(self, entry):
        if entry.widget.get() != "":
            entry.widget.config(bg="white")
        else:
            entry.widget.config(bg="red")

    def useCorrectFieldBaseData(self, snippetData, snippetVar):
        if snippetData != '':
            return [snippetData, False]
        elif snippetVar != '':
            return [snippetVar, True]
        else:
            return ["baseData", False]

    def addFirstField(self):
        firstGridRow = 1
        rowItems = []
        newSelectionRow = tkinter.ttk.Checkbutton(self.fieldTable)
        newSelectionRow.state(['!alternate'])
        rowItems.append(newSelectionRow)
        newSelectionRow.grid(row=firstGridRow, column=0)
        for columnIndex in range(1, 3):  # Columns
            newTableRow = Entry(self.fieldTable)
            newTableRow.bind("<KeyRelease>", lambda event=self: self.refreshVisualEntryStatus(event))
            rowItems.append(newTableRow)
            newTableRow.grid(row=firstGridRow, column=columnIndex)
        rowItems[1].focus()
        self.rows.append(rowItems)

    def deleteField(self):
        for rowIndex, row in reversed(list(enumerate(self.rows))):
            deleteCheckBoxState = row[0]
            if deleteCheckBoxState.instate(['selected']):
                for cell in row:
                    cell.destroy()
                self.rows.pop(rowIndex)
        self.getTemporaryFields()
        self.refreshFieldTableAndListAfterExec()

    def getTemporaryFields(self):
        for rowIndex in range(len(self.rows)):
            technicalFieldName = self.rows[rowIndex][1].get()
            labelDe = self.rows[rowIndex][2].get()
            if technicalFieldName != "" or labelDe != "":
                field = Field(technicalFieldName, labelDe)
                self.tempFieldList.append(field)

    def refreshFieldTableAndListAfterExec(self):
        self.fieldTable.destroy()
        self.fieldTable = []
        self.rows = []
        self.fieldTable = Frame(self.snippetFieldTab)
        self.fieldTable.grid(row=3, column=0)
        self.createTableHeader()
        self.addTempEditableFields()
        self.tempFieldList = []

    def disableSnippetInputForMetaAndMasterData(self):
        if self.snippetTypeInput.get() != "generic_table":
            self.snippetDataInput.delete(0, 'end')
            self.snippetDataInput.config(state='disabled')
            self.snippetLoopVarInput.config(state='normal')

    def checkSnippetInputFields(self):
        if len(self.snippetDataInput.get()) > 0:
            self.snippetLoopVarInput.config(state='disabled')
        else:
            self.snippetLoopVarInput.config(state='normal')
        if len(self.snippetLoopVarInput.get()) > 0:
            self.snippetDataInput.config(state='disabled')
        elif self.snippetTypeInput.get() == "generic_table":
            self.snippetDataInput.config(state='normal')

    def createSnippetSettings(self):
        saveSnippetButton = Button(self.snippetDataTab,
                                   text='Save snippet and fields',
                                   fg="white",
                                   bg="green",
                                   font="bold",
                                   command=self.saveSnippet)
        saveSnippetButton.grid(row=0, column=0)
        self.snippetTypeLabel = Label(self.snippetDataTab, text="Snippet type", font='bold')
        self.snippetNameLabel = Label(self.snippetDataTab, text="Snippet name", font='bold')
        self.snippetDataLabel = Label(self.snippetDataTab, text="Snippetdata", font='bold')
        self.snippetLoopVarLabel = Label(self.snippetDataTab, text="Or loop variable", font='bold')
        self.snippetTypeInput = Combobox(self.snippetDataTab, state="readonly")
        self.snippetTypeInput['values'] = ("generic_table", "generic_masterdata", "generic_metadata")
        self.snippetTypeInput.set("generic_table")
        self.snippetTypeInput.focus()
        self.snippetTypeInput.bind("<FocusOut>", lambda v=self: self.disableSnippetInputForMetaAndMasterData())
        self.snippetTypeInput.bind("<Leave>", lambda v=self: self.disableSnippetInputForMetaAndMasterData())
        self.snippetNameInput = Entry(self.snippetDataTab, width=55)
        self.snippetDataInput = Entry(self.snippetDataTab, width=55)
        self.snippetDataInput.bind("<KeyRelease>", lambda v=self: self.checkSnippetInputFields())
        self.snippetLoopVarInput = Entry(self.snippetDataTab, width=55)
        self.snippetLoopVarInput.bind("<KeyRelease>", lambda v=self: self.checkSnippetInputFields())
        if self.editableSnippet is not None:
            self.snippetTypeInput.set(self.editableSnippet.snippetType)
            self.snippetNameInput.insert(0, self.editableSnippet.snippetName)
            self.snippetNameOfEditableBefore = self.editableSnippet.snippetName
            if self.editableSnippet.isNoSnippetInput:
                self.snippetLoopVarInput.insert(0, self.editableSnippet.snippetData)
            else:
                self.snippetDataInput.insert(0, self.editableSnippet.snippetData)
        self.snippetTypeLabel.grid(row=1, column=0, pady=(5, 5))
        self.snippetTypeInput.grid(row=1, column=1)
        self.snippetNameLabel.grid(row=2, column=0, pady=(0, 5))
        self.snippetNameInput.grid(row=2, column=1)
        self.snippetDataLabel.grid(row=3, column=0, pady=(0, 5))
        self.snippetDataInput.grid(row=3, column=1)
        self.snippetLoopVarLabel.grid(row=4, column=0, pady=(0, 5))
        self.snippetLoopVarInput.grid(row=4, column=1)

    def createFieldsTab(self):
        createButton = Button(self.snippetFieldTab,
                              text='Add field',
                              fg="white",
                              bg="green",
                              font="bold",
                              command=self.addField)
        createButton.grid(row=0, column=0)
        deleteButton = Button(self.snippetFieldTab,
                              text='Delete field',
                              fg="white",
                              bg="red",
                              font="bold",
                              command=self.deleteField)
        deleteButton.grid(row=0, column=1)

    def createTableHeader(self):
        deleteColumnLabel = Label(self.fieldTable, text="Delete selection", font='bold')
        deleteColumnLabel.grid(row=0, column=0)

        fieldContentLabel = Label(self.fieldTable, text="Technical fieldname", font='bold')
        fieldContentLabel.grid(row=0, column=1)

        columnLabelDe = Label(self.fieldTable, text="Label DE", font='bold')
        columnLabelDe.grid(row=0, column=2)
