import os
import platform
import re
import shutil
from tkinter import messagebox


class GenericSnippet:
    def __init__(self, snippetType, snippetName, snippetData, isNoSnippetInput, snippetFields):
        self.snippetType = snippetType
        self.snippetName = snippetName
        self.snippetData = snippetData
        self.isNoSnippetInput = isNoSnippetInput
        self.snippetFields = snippetFields

class CreateBasicReportComponents:
    def __init__(self, applicationPath, reportId, reportName, reportLocation, reportEntity, reportRoles, snippetList):
        self.reportId = reportId
        self.applicationPath = applicationPath
        self.reportName = reportName
        self.reportLocation = reportLocation
        self.reportEntity = reportEntity
        self.reportRoles = reportRoles
        self.snippetList = snippetList

    def execReportGenerator(self, outputPath):
        targetPath = outputPath + "/output"
        reportPathWithFields = targetPath + "/report/outputtemplate/fields"
        if os.path.exists(targetPath + "/report"):
            shutil.rmtree(targetPath + "/report")
        try:
            os.makedirs(reportPathWithFields)
        except OSError as e:
            messagebox.showerror("Can't create Output / No Permission", e)
            return
        self.createOutputTemplateFile(targetPath)
        self.createFieldsAsFilesOfSnippet(targetPath)
        self.createHivemoduleFile(targetPath)
        self.createPropertyFile(targetPath)
        if "Linux" in platform.system():
            os.system("nohup nautilus " + targetPath + " &") # & is needed to don't freeze the app, while nautilus is open
        else:
            os.system("open " + targetPath)

    def withQuotationMark(self, attribute):
        return '"' + str(attribute) + '"'

    def createReportInHivemodule(self):
        reportEntry = self.readFile(self.applicationPath + "/templates/report.xml")
        return reportEntry.format(uniqueId=self.reportId,
                                  label=self.reportId,
                                  roles=self.reportRoles,
                                  reportPlacement=self.reportLocation,
                                  entityModel=self.reportEntity)

    def createOutputTemplateInHivemodule(self):
        outputTemplateEntry = self.readFile(self.applicationPath + "/templates/outputtemplate.xml")
        return outputTemplateEntry.format(reportId=self.reportId)

    def createGenericSnippet(self, snippetType, snippetName, snippetData, isNoSnippetInput):
        if snippetType != "":
            genericSnippet = ""
            genericSnippetEntry = self.readFile(self.applicationPath + "/templates/snippetdirectivetemplate.ftl")
            genericSnippet = genericSnippetEntry.format(snippetType=snippetType,
                                                            snippetName=snippetName,
                                                            snippetData=snippetData)
            if isNoSnippetInput:
                return genericSnippet.replace(" snippetData=" + snippetData, "")
            return genericSnippet
        else:
            return ""

    def createLanguageKeys(self):
        languageKeys = []
        languageKeys.append("report." + self.reportId + "=" + self.reportName)
        languageKeysEntry = self.readFile(self.applicationPath + "/templates/textressourcetemplate.properties")
        for snippet in self.snippetList:
            for field in snippet.snippetFields:
                fieldName = self.createUseFullFieldName(field.technicalFieldName)
                languageKeys.append("\n")
                languageKeys.append(languageKeysEntry.format(reportId=self.reportId, field=fieldName, labelDe=field.labelDe))
        return "".join(languageKeys)

    def createPropertyFile(self, targetPath):
        file = open(targetPath + "/languages.properties", "w+")
        file.write(self.createLanguageKeys())
        file.close()

    def createFieldsInHivemodule(self):
        hiveModuleFields = []
        outputTemplateFieldEntry = self.readFile(self.applicationPath + "/templates/outputtemplate_field.xml")
        snippetIndex = 0
        for snippet in self.snippetList:
            snippetIndex += 1
            fieldIndex = 0
            sortingIndex = 0
            if len(snippet.snippetFields) > 0:
                for field in snippet.snippetFields:
                    fieldIndex += 1
                    if field.technicalFieldName != "":
                        outputTemplateField = outputTemplateFieldEntry
                        sortingIndex += 10
                        fieldName = self.createUseFullFieldName(field.technicalFieldName)
                        hiveModuleFields.append(outputTemplateField.format(reportId=self.reportId,
                                                                              uniqueId=self.reportId + "." + fieldName,
                                                                              snippetId=snippet.snippetName,
                                                                              title="report." + self.reportId + "." + fieldName,
                                                                              sortingIndex=sortingIndex,
                                                                              outputTemplateField=self.reportId + "." + fieldName))
                        hiveModuleFields.append("\n" if fieldIndex != len(snippet.snippetFields) else "")
                hiveModuleFields.append("\n\n" if snippetIndex != len(self.snippetList) else "")
        return "".join(hiveModuleFields)


    def createFieldsAsFilesOfSnippet(self, targetPath):
        for snippet in self.snippetList:
            if len(snippet.snippetFields) > 0:
                for field in snippet.snippetFields:
                    if field.technicalFieldName != "":
                        fieldName = self.createUseFullFieldName(field.technicalFieldName)
                        file = open(targetPath + "/report/outputtemplate/fields/" + self.reportId + "." + fieldName + ".ftl", "w+")
                        if not snippet.isNoSnippetInput:
                            file.write("${currentData." + field.technicalFieldName + "}")
                        else:
                            file.write("${" + snippet.snippetData + "." + field.technicalFieldName + "}")
                        file.close()

    def createUseFullFieldName(self, technicalFieldName):
        if technicalFieldName.startswith("rel") or ".rel" in technicalFieldName:
            regexMultipleRelations = "[^(?!.*?\.rel).*][a-zA-Z0-9_]+.[a-zA-Z0-9_]+$"
            relationSubPart = re.search(regexMultipleRelations, technicalFieldName)
            entityAndAttr = relationSubPart.group()
            if ".label" in entityAndAttr:
                fieldName = entityAndAttr.replace(".label", "")
            else:
                fieldName = entityAndAttr
            return fieldName.lower()
        else:
            return technicalFieldName

    def createOutputTemplateCode(self):
        outputTemplateCode = []
        outputTemplateCode.append("""<div class="document-wrapper">\n""")
        for snippet in self.snippetList:
            outputTemplateCode.append(self.createGenericSnippet(snippet.snippetType,
                                                                snippet.snippetName,
                                                                snippet.snippetData,
                                                                snippet.isNoSnippetInput) + "\n")
        outputTemplateCode.append("</div>")
        return "".join(outputTemplateCode)

    def createOutputTemplateFile(self, targetPath):
        file = open(targetPath +"/report/outputtemplate/" + self.reportId + ".ftl", "w+")
        outputTemplateCode = self.createOutputTemplateCode()
        file.write(outputTemplateCode)
        file.close()

    def createHivemoduleFile(self, targetPath):
        reportContributionStartTag = "\n\t<contribution configuration-id=\"nice2.reporting.Reports\">\n"
        outputTemplateStartTag = "\t<contribution configuration-id=\"nice2.reporting.OutputTemplates\">\n"
        outputTemplateFieldStartTag = "\t<contribution configuration-id=\"nice2.reporting.OutputTemplateFields\">\n"
        contributionEndTag = "\n\t</contribution>\n\n"

        file = open(targetPath +"/report/hivemodule.xml", "w+")
        file.write("<xml>")
        reportContent = self.createReportInHivemodule()
        if reportContent != '':
            file.write(reportContributionStartTag)
            file.write("\t" + reportContent)
            file.write(contributionEndTag)
        outputTemplateContent = self.createOutputTemplateInHivemodule()
        if outputTemplateContent != '':
            file.write(outputTemplateStartTag)
            file.write("\t\t" + outputTemplateContent)
            file.write(contributionEndTag)
        fieldsContent = self.createFieldsInHivemodule()
        if fieldsContent != '':
            file.write(outputTemplateFieldStartTag)
            file.write(fieldsContent)
            file.write(contributionEndTag + "\n")
        file.write("</xml>")
        file.close()

    def readFile(self, filePath):
        file = open(filePath, "r")
        fileContent = file.read()
        file.close()
        return fileContent
