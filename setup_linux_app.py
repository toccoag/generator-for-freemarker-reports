import os

def createDesktopIcon(absoluteTargetPath):
    desktopConfigFile = open(sourceFolder + "/report-generator-template.desktop")
    desktopIconConfigTemplate = desktopConfigFile.read()
    desktopIconConfig = desktopIconConfigTemplate.format(absoluteTargetPath=absoluteTargetPath + "/dist/report-generator-app",
                                                         iconPath=absoluteTargetPath)
    os.system('echo "' + desktopIconConfig + '" > ' + "~/.local/share/applications/report-generator.desktop")
    desktopConfigFile.close()

def setUpFromTargetPath(fileDataStr):
    os.chdir(applicationFolder)
    absoluteTargetPath = os.getcwd()
    createDesktopIcon(absoluteTargetPath)
    os.system("pyinstaller -y  " + fileDataStr + " " + sourceMainFile)

# "../report_generator_august_02/"
sourceFolder = input("Enter relative path to source-code from your installation-target: \n")
sourceImplFolder = sourceFolder + "/impl/"
sourceMainFile = sourceImplFolder + "/report-generator-app.py"
# "../report-app/"
applicationFolder = input("Enter relative path to installation-target from the source-code: \n")
if not os.path.exists(applicationFolder):
    os.mkdir(applicationFolder)
files = []
os.system("ls")
templateFiles = os.popen("ls impl/templates").read().split("\n")
for file in templateFiles:
    if file != "":
        files.append(' --add-data "')
        files.append(sourceImplFolder + "templates/" + file + ":" + "templates")
        files.append('"')
fileDataStr = "".join(files)
os.system("cp tocco.ico " + applicationFolder + "/tocco.ico")
setUpFromTargetPath(fileDataStr)


