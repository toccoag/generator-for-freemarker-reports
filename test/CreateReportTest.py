import unittest
import os
from impl.CreateBasicReportComponents import CreateBasicReportComponents
from impl.CreateBasicReportComponents import GenericSnippet
from impl.CreateSnippetApp import Field
from impl.CreateYamlApp import YamlTextResource, YamlCodeCreator

class CreateReportTest(unittest.TestCase):

    def testReportComponents(self):
        report = self.createTestReport()
        reportEntryExpected = self.readFile("expected_results/report_reference.xml")
        reportEntryActual = report.createReportInHivemodule()
        outputTemplateEntryExpected = self.readFile("expected_results/outputtemplate_reference.xml")
        outputTemplateEntryActual = report.createOutputTemplateInHivemodule()
        fieldEntriesExpected = self.readFile("expected_results/outputtemplatefield_reference.xml")
        fieldEntriesActual = report.createFieldsInHivemodule()
        languageKeysExpected = self.readFile("expected_results/languages.properties")
        languageKeysActual = report.createLanguageKeys()
        outputTemplateCodeExpected = self.readFile("expected_results/outputtemplate_code.ftl")
        outputTemplateCodeActual = report.createOutputTemplateCode()
        self.assertEqual(reportEntryExpected, reportEntryActual)
        self.assertEqual(outputTemplateEntryExpected, outputTemplateEntryActual)
        self.assertEqual(fieldEntriesExpected, fieldEntriesActual)
        self.assertEqual(languageKeysExpected, languageKeysActual)
        self.assertEqual(outputTemplateCodeExpected, outputTemplateCodeActual)

    def testYamlInserts(self):
        currentDir = os.curdir + "/../impl"
        yamlList = self.createYamlTestData()
        yamlCreator = YamlCodeCreator(yamlList)
        yamlCodeExpected = self.readFile("expected_results/yamlvalues.yaml")
        yamlCodeActual = yamlCreator.createYamlResource(currentDir)
        yamlTextExpected = self.readFile("expected_results/yamltext.properties")
        yamlTextActual = yamlCreator.createYamlPropertyResource(currentDir)
        self.assertEqual(yamlCodeExpected, yamlCodeActual)
        self.assertEqual(yamlTextExpected, yamlTextActual)

    def createTestReport(self):
        currentDir = os.curdir + "/../impl"
        snippetFieldsOne = [Field("relEvent.label", "Veranstaltung"),
                            Field("duration", "Dauer")]
        snippetFieldsTwo = [Field("relCampaign.relDonation.label", "Spende"),
                            Field("relCampaign.relDonation.abbreviation", "Abkürzung")]

        snippetOne = GenericSnippet("generic_table", "table_event", "baseData", False, snippetFieldsOne)
        snippetTwo = GenericSnippet("generic_masterdata", "masterdata_donation", "", True, snippetFieldsTwo)

        snippetList = [snippetOne, snippetTwo]
        return CreateBasicReportComponents(currentDir,
                                          "absence_list",
                                          "Absenzenliste",
                                          "list, detail",
                                          "Event",
                                          "eventmanager, eventguest",
                                          snippetList)

    def createYamlTestData(self):
        yamlDataOne = YamlTextResource("membership_title", "membership", "Mitgliedschaft - Titel", "Mitglied")
        yamlDataTwo = YamlTextResource("event_campaign", "event", "Veranstaltung - Kampagne", "Kampagne für Veranstaltung")
        yamlList = [yamlDataOne, yamlDataTwo]
        return yamlList



    def readFile(self, filePath):
        file = open(filePath, "r")
        fileContent = file.read()
        file.close()
        return fileContent

if __name__ == '__main__':
    unittest.main()